{-# LANGUAGE OverloadedStrings #-}
import           Control.Monad.State (get, put)
import           Yi
import           Yi.Fuzzy (fuzzyOpen)
import qualified Yi.Keymap.Vim        as V2
import qualified Yi.Keymap.Vim.Common as V2
import qualified Yi.Keymap.Vim.Utils  as V2

import Yi.Doshitan.Keymap.Vim.Utils (v2KeymapSet, nmap, imap, nmap')

main = yi myConfig

myConfig = defaultVimConfig
  { modeTable = fmap (onMode prefIndent) (modeTable defaultVimConfig)
  , defaultKm = myKeymap
  , startActions = [EditorA (do
      e <- get
      put e { maxStatusHeight = 30 })]
  }

prefIndent :: Mode syntax -> Mode syntax
prefIndent m = case modeName m of
                "Makefile" -> m
                _          -> m { modeIndentSettings = smallIndent (modeIndentSettings m)}

smallIndent :: IndentSettings -> IndentSettings
smallIndent settings = settings { expandTabs = True
                                , shiftWidth = 2
                                , tabSize = 2
                                }

myKeymap = v2KeymapSet myBindings

myBindings eval =
  [ nmap ";" (eval ":")
  , nmap' "<C-p>" fuzzyOpen
  ]
